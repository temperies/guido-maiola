package com.nisum.guidomaiola.live;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.nisum.guidomaiola.controller.CheckController;
import com.nisum.guidomaiola.model.NisumUser;
import com.nisum.guidomaiola.model.UserToken;
import org.junit.Before;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.UUID;

import static com.nisum.guidomaiola.controller.RegistrationRestController.CTX;
import static com.nisum.guidomaiola.controller.RegistrationRestController.REGISTRATION_URL;
import static com.nisum.guidomaiola.utils.FactoryUtils.newUserDto;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class RegistrationRestControllerLiveTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private MockMvc mockMvc;
    private String token;

    @Autowired
    private ObjectMapper objectMapper;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        NisumUser user = new NisumUser();
        user.setEmail(UUID.randomUUID().toString() + "@myemail.cl");
        user.setPassword(UUID.randomUUID().toString());
        user.setName("Full Name");


        entityManager.persist(user);
        token = UUID.randomUUID().toString();
        UserToken userToken = new UserToken(token, user);
        userToken.setExpirationDate(Date.from(Instant.now().plus(2, ChronoUnit.DAYS)));

        entityManager.persist(userToken);

        /*
            flush managed entities to the database to populate identifier field
         */
        entityManager.flush();
        entityManager.clear();
    }

    @Test
    @Tag("live")
    public void testRegistrationValidation() throws Exception {

        ResultActions resultActions = this.mockMvc.perform(post(CTX + REGISTRATION_URL)
            .contentType("application/json")
            .content(objectMapper.writeValueAsString(newUserDto())));
        resultActions.andExpect(status().is4xxClientError());
    }

}