package com.nisum.guidomaiola.model;


import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Set;


@Entity
@Table(name = "countries")
public class Country {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @Column(name = "code", unique = true)
  private String code;

  @NotBlank
  @Column(name = "name", unique = true)
  private String name;

  @OneToMany(mappedBy = "country", orphanRemoval=true)
  private Set<City> cities;



  public long getId() {

    return id;
  }



  public void setId(long id) {

    this.id = id;
  }



  public String getCode() {

    return code;
  }



  public void setCode(String code) {

    this.code = code;
  }



  public String getName() {

    return name;
  }



  public void setName(String name) {

    this.name = name;
  }



  public Set<City> getCities() {

    return cities;
  }



  public void setCities(Set<City> cities) {

    this.cities = cities;
  }



  public boolean add(City city) {

    return cities.add(city);
  }
}
