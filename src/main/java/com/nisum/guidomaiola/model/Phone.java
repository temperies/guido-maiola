package com.nisum.guidomaiola.model;


import javax.persistence.*;


@Entity
@Table(name = "phones")
public class Phone {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  private String number;

  @ManyToOne
  private NisumUser user;

  @OneToOne()
  @JoinColumn(name = "countrycode", nullable = false)
  private Country country;

  @OneToOne()
  @JoinColumn(name = "cityCode", nullable = false)
  private City cityCode;



  public long getId() {

    return id;
  }



  public void setId(long id) {

    this.id = id;
  }



  public String getNumber() {

    return number;
  }



  public void setNumber(String number) {

    this.number = number;
  }



  public NisumUser getUser() {

    return user;
  }



  public void setUser(NisumUser user) {

    this.user = user;
  }



  public Country getCountry() {

    return country;
  }



  public void setCountry(Country country) {

    this.country = country;
  }



  public City getCityCode() {

    return cityCode;
  }



  public void setCityCode(City cityCode) {

    this.cityCode = cityCode;
  }
}
