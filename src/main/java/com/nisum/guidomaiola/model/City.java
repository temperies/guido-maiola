package com.nisum.guidomaiola.model;


import javax.persistence.*;


@Entity
@Table(name = "cities")
public class City {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @Column(name = "code", unique = true)
  private String code;

  private String name;

  @JoinColumn(name = "countrycode")
  @OneToOne(fetch = FetchType.LAZY)
  private Country country;



  public long getId() {

    return id;
  }



  public void setId(long id) {

    this.id = id;
  }



  public String getCode() {

    return code;
  }



  public void setCode(String code) {

    this.code = code;
  }



  public String getName() {

    return name;
  }



  public void setName(String name) {

    this.name = name;
  }



  public Country getCountry() {

    return country;
  }



  public void setCountry(Country country) {

    this.country = country;
  }
}
