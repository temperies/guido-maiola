package com.nisum.guidomaiola.service;


import com.nisum.guidomaiola.dto.PhoneDto;
import com.nisum.guidomaiola.dto.UserDto;
import com.nisum.guidomaiola.exception.TokenNotFoundException;
import com.nisum.guidomaiola.exception.UserAlreadyExistException;
import com.nisum.guidomaiola.model.*;
import com.nisum.guidomaiola.repository.UserRepository;
import com.nisum.guidomaiola.repository.UserTokenRepository;
import com.nisum.guidomaiola.security.TokenManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;


@Service
@Transactional
public class UserServiceImpl implements UserService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Autowired
  private UserTokenRepository tokenRepository;

  @Override public UserToken registerNewUserAccount(UserDto accountDto) {

    if (userRepository.existsByEmail(accountDto.getEmail())) {
      throw new UserAlreadyExistException("There is an account with that email address: " + accountDto.getEmail());
    }
    final NisumUser nisumUser = convertToEntity(accountDto);
    userRepository.save(nisumUser);
    return createTokenForUser(nisumUser, TokenManager.generateToken(accountDto.getEmail()));
  }

  @Override
  public NisumUser getUser(final String token) {
    final UserToken userToken = tokenRepository.findByToken(token);
    if (token != null) {
        return userToken.getUser();
    }
    return null;
  }

  @Override
  public NisumUser findUserByEmail(String email) {
    return userRepository.findByEmail(email).orElse(null);
  }

  @Override
  public String validateUserToken(String token) {
    final UserToken userToken = tokenRepository.findByToken(token);
    if (userToken == null) {
      return TOKEN_INVALID;
    }

    final NisumUser user = userToken.getUser();
    final Calendar cal = Calendar.getInstance();
    if ((userToken.getExpirationDate()
            .getTime() - cal.getTime()
            .getTime()) <= 0) {
      tokenRepository.delete(userToken);
      return TOKEN_EXPIRED;
    }

    user.setActive(true);
    userRepository.save(user);
    return TOKEN_VALID;
  }

  @Override
  public UserToken createTokenForUser(NisumUser user, String token) {
    final UserToken myToken = new UserToken(token, user);
    return tokenRepository.save(myToken);
  }

  @Override
  public UserToken getUserToken(String token) {
    return tokenRepository.findByToken(token);
  }

  @Override
  public UserToken generateNewUserToken(String existingUserToken) {

    UserToken userToken = tokenRepository.findByToken(existingUserToken);

    if (userToken == null) {
      throw new TokenNotFoundException("Unable to find the token " + existingUserToken);
    }
    final String generateToken = TokenManager.generateToken(userToken.getUser().getEmail());

    userToken.updateToken(generateToken);
    userToken = tokenRepository.save(userToken);
    return userToken;
  }

  private NisumUser convertToEntity(UserDto userDto) {
    final NisumUser user = new NisumUser();

    user.setName(userDto.getName());
    user.setPassword(passwordEncoder.encode(userDto.getPassword()));
    user.setEmail(userDto.getEmail());
    if (Objects.nonNull(userDto.getPhones())) {
      user.setPhones(userDto.getPhones().stream()
          .map(this::convertToEntity)
          .collect(Collectors.toSet()));
    }

    user.setActive(true);

    return user;
  }

  private Phone convertToEntity(PhoneDto phoneDto) {

    Country country = new Country();
    country.setCode(phoneDto.getCountryCode());

    City city = new City();
    city.setCode(phoneDto.getCityCode());

    Set<City> citySet = new HashSet<>();
    citySet.add(city);

    country.setCities(citySet);

    Phone phone = new Phone();
    phone.setNumber(phoneDto.getNumber());

    phone.setCityCode(city);
    phone.setCountry(country);

    return phone;
  }
}
