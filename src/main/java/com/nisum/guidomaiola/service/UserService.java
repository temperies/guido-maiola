package com.nisum.guidomaiola.service;

import com.nisum.guidomaiola.dto.UserDto;
import com.nisum.guidomaiola.model.NisumUser;
import com.nisum.guidomaiola.model.UserToken;

public interface UserService {
	String TOKEN_INVALID = "invalidToken";
	String TOKEN_EXPIRED = "expired";
	String TOKEN_VALID = "valid";

	UserToken registerNewUserAccount(UserDto accountDto);

	NisumUser getUser(String token);

	NisumUser findUserByEmail(String email);

	String validateUserToken(String token);

	UserToken createTokenForUser(NisumUser user, String token);

	UserToken getUserToken(String token);

	UserToken generateNewUserToken(String existingUserToken);
}
