package com.nisum.guidomaiola.security;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.nisum.guidomaiola.model.NisumUser;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.nisum.guidomaiola.security.TokenManager.TOKEN_PREFIX;
import static com.nisum.guidomaiola.security.TokenManager.generateToken;

public class MyAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

  public static final String AUTH_HEADER_KEY = "Authorization";

  private final AuthenticationManager authenticationManager;

  public MyAuthenticationFilter(AuthenticationManager authenticationManager) {
    this.authenticationManager = authenticationManager;
  }

  @Override
  public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
    try {
      NisumUser user = new ObjectMapper().readValue(request.getInputStream(), NisumUser.class);
      return authenticationManager.authenticate(SecureUserFactory.toAuthenticationToken(user));
    } catch (IOException e) {
      throw new AuthenticationCredentialsNotFoundException("Failed to resolve authentication credentials", e);
    }
  }

  @Override
  protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                FilterChain chain, Authentication authResult) {

    String username;
    if (authResult.getPrincipal() instanceof NisumUser) {
      username = ((NisumUser)authResult.getPrincipal()).getEmail();
    }
    else {
      username = authResult.getName();
    }
    logger.debug("Creating new persistent login for user " + username);
    response.addHeader(AUTH_HEADER_KEY, TOKEN_PREFIX + generateToken(username));
  }
}
