package com.nisum.guidomaiola.dto;


import com.nisum.guidomaiola.validation.ValidEmail;
import com.nisum.guidomaiola.validation.ValidPassword;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

public class UserDto {

  @NotNull(message = "User name can not be null")
  @NotBlank(message = "User name required")
  private String name;

  @ValidEmail
  private String email;

  @ValidPassword
  private String password;

//  @NotNull
  @Size(min = 1)
  private String matchingPassword;

  @Valid
  private Set<PhoneDto> phones;

  public String getName() {

    return name;
  }



  public void setName(String name) {

    this.name = name;
  }



  public String getEmail() {

    return email;
  }



  public void setEmail(String email) {

    this.email = email;
  }



  public String getPassword() {

    return password;
  }



  public String getMatchingPassword() {

    return matchingPassword;
  }



  public void setMatchingPassword(String matchingPassword) {

    this.matchingPassword = matchingPassword;
  }



  public void setPassword(String password) {

    this.password = password;
  }

  public Set<PhoneDto> getPhones() {
    return phones;
  }

  public void setPhones(Set<PhoneDto> phones) {
    this.phones = phones;
  }
}
