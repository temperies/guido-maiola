package com.nisum.guidomaiola.repository;


import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nisum.guidomaiola.model.NisumUser;


@Repository
public interface UserRepository extends JpaRepository<NisumUser, UUID> {

  boolean existsByEmail(String email);

  Optional<NisumUser> findByEmail(String email);

}
