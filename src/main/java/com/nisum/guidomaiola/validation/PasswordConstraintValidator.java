package com.nisum.guidomaiola.validation;


import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordConstraintValidator implements ConstraintValidator<ValidPassword, String> {

    private static final String PASSWORD_PATTERN = "^(?=(.*\\d){2})(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}$";
    private static final Pattern PATTERN = Pattern.compile(PASSWORD_PATTERN);

    @Override
    public boolean isValid(final String password, final ConstraintValidatorContext context) {
        return (validatePassword(password));
    }
    private boolean validatePassword(final String password) {
        if (StringUtils.isEmpty(password)) {
            return false;
        }
        Matcher matcher = PATTERN.matcher(password);
        return matcher.matches();
    }
}
