package com.nisum.guidomaiola.config;

import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.nisum.guidomaiola.security.MyAuthenticationFilter;
import com.nisum.guidomaiola.security.MyAuthorizationFilter;
import com.nisum.guidomaiola.security.MyUserDetailsService;

import static com.nisum.guidomaiola.controller.RegistrationRestController.CTX;
import static com.nisum.guidomaiola.controller.RegistrationRestController.REGISTRATION_URL;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

@Order(2)
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	private final MyUserDetailsService userDetailsService;

	public SecurityConfiguration(MyUserDetailsService userDetailsService) {
		this.userDetailsService = userDetailsService;
	}

	protected @Override void configure(HttpSecurity http) throws Exception {
		// @formatter:off
		http.cors().and().csrf().disable().authorizeRequests().antMatchers(CTX + REGISTRATION_URL).permitAll()
				.antMatchers("/invalidSession*").anonymous().anyRequest().authenticated().and()
				.addFilter(new MyAuthenticationFilter(authenticationManager()))
				.addFilter(new MyAuthorizationFilter(authenticationManager())).sessionManagement()
				.sessionCreationPolicy(STATELESS);
		// @formatter:on
	}

	@Override
	public void configure(final WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/resources/**");
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return PasswordEncoderFactories.createDelegatingPasswordEncoder();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}

	@Bean
	public CorsConfigurationSource corsConfigurationSource() {

		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
		return source;
	}
}
