package com.nisum.guidomaiola.exception;

public final class TokenNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public TokenNotFoundException() {
        super();
    }

    public TokenNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public TokenNotFoundException(final String message) {
        super(message);
    }

    public TokenNotFoundException(final Throwable cause) {
        super(cause);
    }

}
