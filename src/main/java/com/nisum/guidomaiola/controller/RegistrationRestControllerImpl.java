package com.nisum.guidomaiola.controller;


import com.nisum.guidomaiola.dto.UserDto;
import com.nisum.guidomaiola.dto.UserRegisteredDto;
import com.nisum.guidomaiola.model.NisumUser;
import com.nisum.guidomaiola.model.UserToken;
import com.nisum.guidomaiola.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Objects;

@CrossOrigin
@RestController
public class RegistrationRestControllerImpl implements RegistrationRestController {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private UserService userService;

    @Override public UserRegisteredDto registerUserAccount(@Valid UserDto accountDto,
        HttpServletRequest request) {
        logger.debug("Registering user account with information: {}", accountDto);

        final UserToken userToken = userService.registerNewUserAccount(accountDto);
        return toDto(userToken);
    }

    private UserRegisteredDto toDto(UserToken userToken) {
        if (Objects.nonNull(userToken)) {
            if (Objects.nonNull(userToken.getUser())) {
                final NisumUser user = userToken.getUser();
                return new UserRegisteredDto(user.getId(), user.getCreationDate().toString(),
                    user.getLastModifiedDate().toString(), user.getLastLoginDate() != null ?
                    user.getLastLoginDate().toString() : null, userToken.getToken(),
                    user.isActive());
            }
            return new UserRegisteredDto(userToken.getToken());

        }
        return null;
    }
}
